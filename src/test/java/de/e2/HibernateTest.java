package de.e2;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.graph.GraphParser;
import org.hibernate.graph.RootGraph;
import org.hibernate.query.Query;
import org.hibernate.stat.SessionStatistics;
import org.junit.jupiter.api.*;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

public class HibernateTest {

    private SessionFactory sessionFactory;
    private Session session;
    private Transaction transaction;
    private EntityManager entityManager;

    @BeforeEach
    void initSession() {
        Properties properties = new Properties();
        properties.put("hibernate.connection.driver_class", "org.h2.Driver");
        properties.put("hibernate.connection.url", "jdbc:h2:mem:db1;DB_CLOSE_DELAY=-1");
        properties.put("hibernate.connection.username", "sa");
        properties.put("hibernate.connection.password", "");
        properties.put("hibernate.connection.pool_size", 1);
        properties.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.hbm2ddl.auto", "create");

        StandardServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(properties).build();

        MetadataSources metadataSources = new MetadataSources(serviceRegistry);

        metadataSources.addAnnotatedClass(Person.class);
        metadataSources.addAnnotatedClass(Address.class);

        Metadata metadata = metadataSources.getMetadataBuilder()
                .build();

        sessionFactory = metadata.getSessionFactoryBuilder().build();
        entityManager = sessionFactory.createEntityManager();
        session = entityManager.unwrap(Session.class);
        transaction = session.beginTransaction();
    }

    @AfterEach
    void destroySession() {
        transaction.rollback();
        session.close();
    }

    private void addTestData() {
        Person person = new Person(1L);
        Address address = new Address("HH");
        person.setAddress(address);
        session.save(address);
        session.save(person);
        session.flush();
        session.clear();
    }

    @Test
    void testLazyGet() {
        addTestData();

        SessionStatistics statistics = session.getStatistics();
        Person person1 = session.get(Person.class, 1L);
        Assertions.assertEquals(1, statistics.getEntityCount());
        person1.getAddress().getCity();
        Assertions.assertEquals(2, statistics.getEntityCount());
    }

    @Test
    void testLazySelect() {
        addTestData();

        SessionStatistics statistics = session.getStatistics();
        Query<Person> personQuery = session.createQuery("select p from Person p where p.id = :id", Person.class);
        personQuery.setParameter("id",1L);
        Person person1 = personQuery.getSingleResult();
        Assertions.assertEquals(1, statistics.getEntityCount());
        person1.getAddress().getCity();
        Assertions.assertEquals(2, statistics.getEntityCount());
    }

    @Test
    void testFetchGraph() {
        addTestData();

        RootGraph<Person> rootGraph = GraphParser.parse(Person.class, "address2, address", entityManager);

        SessionStatistics statistics = session.getStatistics();
        Person person1 = entityManager.find(Person.class, 1L, Collections.singletonMap("javax.persistence.fetchgraph", rootGraph));
        Assertions.assertEquals(2, statistics.getEntityCount());
    }

    @Test
    void testProjection() {
        addTestData();

        SessionStatistics statistics = session.getStatistics();
        Query personQuery = session.createQuery("select p.id, p.name, p.address.city from Person p");
        List<Object[]> persons = personQuery.getResultList();
        Assertions.assertEquals(1, persons.size());
        Assertions.assertEquals(0, statistics.getEntityCount());
    }

    @Test
    void testProjectionDTO() {
        addTestData();

        SessionStatistics statistics = session.getStatistics();
        Query<PersonDTO> personQuery = session.createQuery("select new de.e2.PersonDTO(p.id, p.name, p.address.city) from Person p", PersonDTO.class);
        List<PersonDTO> persons = personQuery.getResultList();
        Assertions.assertEquals(1, persons.size());
        Assertions.assertEquals(0, statistics.getEntityCount());
    }

}
