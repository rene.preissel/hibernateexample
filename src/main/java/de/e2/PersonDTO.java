package de.e2;

public class PersonDTO {
    private Long id;
    private String name;
    private String city;

    public PersonDTO(Long id, String name, String city) {
        this.id = id;
        this.name = name;
        this.city = city;
    }
}
