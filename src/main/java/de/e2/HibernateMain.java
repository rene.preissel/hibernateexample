package de.e2;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.BootstrapServiceRegistry;
import org.hibernate.boot.registry.BootstrapServiceRegistryBuilder;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.stat.SessionStatistics;

import java.util.Properties;

public class HibernateMain {
    public static void main(String[] args) {
        Properties properties = new Properties();
        properties.put("hibernate.connection.driver_class", "org.h2.Driver");
        properties.put("hibernate.connection.url", "jdbc:h2:mem:db1;DB_CLOSE_DELAY=-1");
        properties.put("hibernate.connection.username", "sa");
        properties.put("hibernate.connection.password", "");
        properties.put("hibernate.connection.pool_size", 1);
        properties.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.hbm2ddl.auto", "create");

        StandardServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(properties).build();

        MetadataSources metadataSources = new MetadataSources(serviceRegistry);

        metadataSources.addAnnotatedClass(Person.class);
        metadataSources.addAnnotatedClass(Address.class);

        Metadata metadata = metadataSources.getMetadataBuilder()
                .build();

        SessionFactory sessionFactory = metadata.getSessionFactoryBuilder().build();
        try(Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            Person person = new Person(1L);
            Address address = new Address("HH");
            person.setAddress(address);
            session.save(address);
            session.save(person);
            session.flush();
            transaction.commit();
        }

        try(Session session = sessionFactory.openSession()) {
            SessionStatistics statistics = session.getStatistics();
            Person person1 = session.get(Person.class, 1L);
            System.out.println(statistics.getEntityCount());
            person1.getAddress().getCity();
            System.out.println(statistics.getEntityCount());
        }
    }
}
