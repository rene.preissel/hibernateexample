plugins {
    java
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation("org.junit.jupiter","junit-jupiter-api","5.7.1")
    testRuntimeOnly("org.junit.jupiter","junit-jupiter-engine","5.7.1")
    implementation("org.hibernate","hibernate-entitymanager","5.4.28.Final")
    implementation("com.h2database","h2","1.4.200")
}

tasks.test {
    useJUnitPlatform()

    maxHeapSize = "1G"
}